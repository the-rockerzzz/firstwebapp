package com.jspider.saptarshiwebappproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.jspider.saptarshiwebappproject.DTO.RegisterDTO;
import com.jspider.saptarshiwebappproject.constant.AppConstant;
import com.jspider.saptarshiwebappproject.registerservice.RegisterService;

//@Component
@Controller
@RequestMapping(AppConstant.FORWARD_SLASH)
public class RegisterController {
	@Autowired
	private RegisterService rs;

	public RegisterController() {
		System.out.println(this.getClass().getSimpleName() + " class object is created");
	}

	@PostMapping(value = AppConstant.SAVE_REGISTER_DETAILS)
//	@RequestMapping(value=AppConstant.SAVE_REGISTER_DETAILS, method=HttpMethod.POST)	
	public ModelAndView saveRegisterDetails(RegisterDTO dto) {
		System.out.println(dto);
		rs.processRegisterDetails(dto);
		return new ModelAndView("index.jsp");
	}
//	public String display(@RequestParam("name") String name1,
//			@RequestParam ("email") String email2,
//			@RequestParam ("contactNumber") String contact3,
//			@RequestParam ("city") String city4,
//			@RequestParam ("country") String country5,
//			@RequestParam ("pincode") String pincode6) {
//		System.out.println(name1+";\n "+email2+";\n "+contact3+";\n "+city4+";\n "+country5+";\n "+pincode6);
//		System.out.println("reached display method");
//		return "display.jsp";
//	}
}
