package com.jspider.saptarshiwebappproject.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
@ComponentScan("com.jspider.saptarshiwebappproject")
public class AppConfig {
	public AppConfig() {
		System.err.println("config");
	}
	
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}
	
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(getDataSource());
		sessionFactory.setPackagesToScan("com.jspider.saptarshiwebappproject.entity");
		sessionFactory.setHibernateProperties(null);
		Properties pro = new Properties();
		pro.setProperty("hibernate.show_sql", "true");
		pro.setProperty("hibernate.hbm2ddl.auto", "update");
		sessionFactory.setHibernateProperties(pro);
		return sessionFactory;
	}
	
	@Bean
	public  DataSource getDataSource() {
		 DriverManagerDataSource source = new DriverManagerDataSource();
		 source.setUrl("jdbc:mysql://localhost:3306/springweb");
		 source.setUsername("root");
		 source.setPassword("root");
		 source.setDriverClassName("com.mysql.jdbc.Driver");
		 return source;
	}
}