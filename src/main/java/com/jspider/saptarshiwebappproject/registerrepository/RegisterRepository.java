package com.jspider.saptarshiwebappproject.registerrepository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.jspider.saptarshiwebappproject.DTO.RegisterDTO;
import com.jspider.saptarshiwebappproject.entity.RegisterEntity;
//@Component
@Repository
public class RegisterRepository {
	@Autowired
	private SessionFactory sf;
	public void save(RegisterEntity registerEntity) {
		Session session = sf.openSession();
		try {
			Transaction transaction = session.beginTransaction();
			session.merge(registerEntity);//exactly works as saveAndUpdate,it only returns an object  
			transaction.commit();
			
		}
		catch(Exception e) {
			
		}
		finally {
			session.close();
		}
	}
}
