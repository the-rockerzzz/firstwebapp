package com.jspider.saptarshiwebappproject.constant;

public interface AppConstant {
	public static String FORWARD_SLASH ="/";
	public static String SAVE_REGISTER_DETAILS ="/saveRegisterDetails";
	public static String REGISTER_INFO="register_Info";
}
