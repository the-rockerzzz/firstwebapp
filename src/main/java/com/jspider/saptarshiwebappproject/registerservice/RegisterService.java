package com.jspider.saptarshiwebappproject.registerservice;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.jspider.saptarshiwebappproject.DTO.RegisterDTO;
import com.jspider.saptarshiwebappproject.entity.RegisterEntity;
import com.jspider.saptarshiwebappproject.registerrepository.RegisterRepository;
//@Component
@Service
public class RegisterService {
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RegisterRepository rp;
	
	
	public void processRegisterDetails(RegisterDTO dto) {
		RegisterEntity registerEntity = modelMapper.map(dto, RegisterEntity.class);
		rp.save(registerEntity);
	}

}
